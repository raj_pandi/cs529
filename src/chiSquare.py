#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 19 03:08:54 2017

@author: Raj, Damion and Dominic
"""
import math
import collections

class chi_square:
    def chi(self,y1,y2):
        s = collections.Counter(y1)
        r = collections.Counter(y2)
        
        expected_n_sum= s['N']
        expected_ei_sum = s['EI']
        expected_ie_sum = s['IE']
        
        obtained_n_sum= r['N']
        obtained_ei_sum = r['EI']
        obtained_ie_sum = r['IE']
        
        diff_n = obtained_n_sum - expected_n_sum
        diff_ei = obtained_ei_sum - expected_ei_sum
        diff_ie = obtained_ie_sum - expected_ie_sum
        
        chiS = (math.pow(diff_n, 2)/expected_n_sum) + (math.pow(diff_ei, 2)/expected_ei_sum) + (math.pow(diff_ie, 2)/expected_ie_sum)
        
        return chiS