import numpy as np
import scipy.stats as stats
import numpy as np

X = stats.chi2.ppf(q = 0.99, # Find the critical value for 95% confidence*
                      df = 118)   # Df = number of variable categories - 1

print("Critical value")
print(X)

#After picking a node

# count of instances A, C, G, T
nA = 996 #sum of As 
nC = 342 #sum of Cs
nG = 328 #sum of Gs
nT = 334 #sum of Ts

#count of classes or target attributes N, E, EI
nN = 1038 #sum of Ns
nEI = 494 #sum of EI
nIE = 468 #sum of IE

total_examples = nN + nEI + nIE # count of examples left after split, should be equal to (nA + nC + nG + nT) and (nN, nEI, nIE)

# Expected proportions of A and class N, EI or IE
EXP_A_N = nA *(nN/total_examples)
EXP_A_EI = nA *(nEI/total_examples)
EXP_A_IE = nA *(nIE/total_examples)

# Expected proportions of C and class N, EI or IE
EXP_C_N = nC *(nN/total_examples)
EXP_C_EI = nC *(nEI/total_examples) 
EXP_C_IE = nC *(nIE/total_examples)

# Expected proportions of G and class N, EI or IE
EXP_G_N = nG *(nN/total_examples)
EXP_G_EI = nG *(nEI/total_examples)
EXP_G_IE = nG *(nIE/total_examples)

# Expected proportion of T and class N, EI or IE
EXP_T_N = nT *(nN/total_examples)
EXP_T_EI = nT *(nEI/total_examples)
EXP_T_IE = nT *(nIE/total_examples)

RC_AN = 252 # real count of A and N
RC_AEI = 278 # real count of A and EI
RC_AIE = 466 # real count of A and IE

# nA = RC_AN + RC_AEI + RC_AIE

RC_CN = 267 # real count of C and N
RC_CEI = 75 # real count of C and EI
RC_CIE = 0 # real count of C and IE

# nC = RC_CN + RC_CEI + RC_CIE

RC_GN = 248 # real count of G and N
RC_GEI = 79 # real count of G and EI
RC_GIE = 1 # real count of G and IE

# nG = RC_GN + RC_GEI + RC_GIE

RC_TN = 271 # real count of T and N
RC_TEI = 61 # real count of T and EI
RC_TIE = 1 # real count of T and IE

# nT = RC_TN + RC_TEI + RC_TIE

#	4 TERMS = INSTANCE A, INSTANCE C, INSTANCE G, INSTANCE T
Class_N = (np.square(RC_AN-EXP_A_N))/EXP_A_N + (np.square(RC_CN-EXP_C_N))/EXP_C_N + (np.square(RC_GN-EXP_G_N))/EXP_G_N + (np.square(RC_TN-EXP_T_N))/EXP_T_N 
Class_EI = (np.square(RC_AEI-EXP_A_EI))/EXP_A_EI + (np.square(RC_CEI-EXP_C_EI))/EXP_C_EI + (np.square(RC_GEI-EXP_G_EI))/EXP_G_EI + (np.square(RC_TEI-EXP_T_EI))/EXP_T_EI 
Class_IE = (np.square(RC_AIE-EXP_A_IE))/EXP_A_IE + (np.square(RC_CIE-EXP_C_IE))/EXP_C_IE + (np.square(RC_GIE-EXP_G_IE))/EXP_G_IE + (np.square(RC_TIE-EXP_T_IE))/EXP_T_IE 

C = Class_N + Class_EI + Class_IE

if C <= X: 
	print("C:", C, "<=", "X:", X)
	print("Accept Ho, the split is by chance")
else: 
	print("C:", C, ">", "X:", X)
	print("Reject Ho, the split is not by chance, means keep building the tree")

# s = np.square(RC_AN-EXP_A_N)
# print(s)

# Ho = The split is by chance

# if C <= X, accept Ho (true), the split is by chance, so bad split, stop and remove node
# if C > X, reject Ho (false), the split is not by chance, hence, it is statistically significant, so continue creating nodes




