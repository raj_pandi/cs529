#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 19 03:08:54 2017

@author: Raj, Damion and Dominic
"""
import math
from collections import Counter
from collections import defaultdict

class gini:
    def acgt_index(self,g,s,ra):
        max_gain = []
        klist = []
        for i in range(len(g)):
            if i == ra :
                k = Counter(g[i])
                continue
            try:
                k = Counter(g[i])
            except KeyError:
                pass
            s = Counter(s)
        
            tot_sum = s['N']+s['EI']+s['IE']
            prob_n = s['N']/tot_sum
            prob_ei = s['EI']/tot_sum
            prob_ie = s['IE']/tot_sum
            acgt = 1 - (math.pow(prob_n, 2) + math.pow(prob_ei, 2) + math.pow(prob_ie, 2))
        
            
            totA_sum = k[('A','N')] + k[('A','IE')] + k[('A','EI')]
            probAn = k[('A','N')]/tot_sum
            probAie = k[('A','IE')]/tot_sum
            probAei = k[('A','EI')]/tot_sum
            aindex = 1 - (math.pow(probAn, 2) + math.pow(probAie, 2) + math.pow(probAei, 2))
        
            
            totC_sum = k[('C','N')] + k[('C','IE')] + k[('C','EI')]
            probCn = k[('C','N')]/tot_sum
            probCie = k[('C','IE')]/tot_sum
            probCei = k[('C','EI')]/tot_sum
            cindex = 1 - (math.pow(probCn, 2) + math.pow(probCie, 2) + math.pow(probCei, 2))
            
          
            totG_sum = k[('G','N')] + k[('G','IE')] + k[('G','EI')]
            probGn = k[('G','N')]/tot_sum
            probGie = k[('G','IE')]/tot_sum
            probGei = k[('G','EI')]/tot_sum
            gindex = 1 - (math.pow(probGn, 2) + math.pow(probGie, 2) + math.pow(probGei, 2))
            
            
            totT_sum = k[('T','N')] + k[('T','IE')] + k[('T','EI')]
            probTn = k[('T','N')]/tot_sum
            probTie =k[('T','IE')]/tot_sum
            probTei = k[('T','EI')]/tot_sum
            tindex = 1 - (math.pow(probTn, 2) + math.pow(probTie, 2) + math.pow(probTei, 2))
            
            
            info_gain = acgt - (totA_sum/tot_sum)*aindex- (totC_sum/tot_sum)*cindex - (totG_sum/tot_sum)*gindex - (totT_sum/tot_sum)*tindex 
            z = (i,info_gain)
            max_gain.append(list(z))
            klist.append(k)
        
        d = defaultdict(list)
        for i in max_gain:
            d[i[0]].append(i[1:])
        return (d,klist)
