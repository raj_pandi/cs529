This uses python3 and the following packages:
import time
import numpy as np
import collections
import sys
import math
import pylab
from collections import Counter
from anytree import *
from entropy_IH import entropy_IF
from ginindex import gini
import networkx as nx
import matplotlib.pyplot as plt
import pandas as pd
import pygraphviz as pgv

Files included: 
dt.py   (main executable)
entropy_IH.py (code to calculate entropy and max gain)
ginindex.py (code to calculate gini)

Requires:  training.csv to be in same directory


Run by either chmod u+x dt.py and ./dt.py or python3 dt.py

dt will ask
Press 1 for entropy_calculation and 2 for gini calculation

enter number 1 or 2 for your selection

dt produces two files:
SedCmd.sed  (A sed script).  
    - To use exec 
        % sed -f SedCmd.sed testing.csv > results
    - Edit results file to add id,class to top of file
    - save and quit for submission
DT.png (Tree image)