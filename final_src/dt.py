#!/usr/local/bin/python3
import time
import numpy as np
import collections
import sys
import math
import pylab
from collections import Counter
from anytree import *
from entropy_IH import entropy_IF
from ginindex import gini
import networkx as nx
import matplotlib.pyplot as plt
import pandas as pd
import pygraphviz as pgv

G = pgv.AGraph(directed=True, strict=False)
h = 1100
o = 1300

#This programs prints to the console all the root attribute of the  split, and also generates the decision tree with filename as DT.png, run "python3 dt.py"
#packages needed  => pygraphviz,networkx,matplotlib and pandas
#our package entropy_IH and ginindex should be in current working directory

plot_edges = []
plot_prob = []
SedString = []
"""y2 into chi comes in the form Counter({('G', 'N'): 84, ('T', 'N'): 43, ('G', 'IE'): 33, ('G', 'EI'): 29, ('C', 'N'): 28, ('C', 'IE'): 14, ('A', 'EI'): 12, ('T', 'EI'): 12, ('A', 'N'): 11, ('C', 'EI'): 11, ('T', 'IE'): 10, ('A', 'IE'): 4})"""

"""This function returns the chisqaure value"""
def chi(y2):
        expected_n_sum= TotalSum['N'] 
        expected_ei_sum = TotalSum['EI']
        expected_ie_sum = TotalSum['IE']

        obtained_n_sum= y2[('A','N')] + y2[('C','N')] +  y2[('G','N')] + y2[('T','N')] 
        obtained_ei_sum = y2[('C','EI')] + y2[('A','EI')] + y2[('G','EI')] + y2[('T','EI')]
        obtained_ie_sum = y2[('C','IE')] + y2[('A','IE')] + y2[('T','IE')] + y2[('G','IE')]

        diff_n = obtained_n_sum - expected_n_sum
        diff_ei = obtained_ei_sum - expected_ei_sum
        diff_ie = obtained_ie_sum - expected_ie_sum

        prob_AN = y2[('A','N')] / obtained_n_sum
        prob_AEI = y2[('A','EI')] / obtained_ei_sum
        prob_AIE = y2[('A','IE')] / obtained_ie_sum

        Ach = {'N': prob_AN,'EI': prob_AEI,'IE': prob_AIE}
        AChoice = max(Ach, key = Ach.get)

        prob_CN = y2[('C','N')] / obtained_n_sum
        prob_CEI = y2[('C','EI')] / obtained_ei_sum
        prob_CIE = y2[('C','IE')] / obtained_ie_sum

        Cch = {'N':prob_CN,'EI':prob_CEI,'IE':prob_CIE}
        CChoice = max(Cch, key = Cch.get)

        prob_GN = y2[('G','N')] / obtained_n_sum
        prob_GEI = y2[('G','EI')] / obtained_ei_sum
        prob_GIE = y2[('G','IE')] / obtained_ie_sum

        Gch = {'N':prob_GN,'EI':prob_GEI,'IE':prob_GIE}
        GChoice = max(Gch, key = Gch.get)

        prob_TN = y2[('T','N')] / obtained_n_sum
        prob_TEI = y2[('T','EI')] / obtained_ei_sum
        prob_TIE = y2[('T','IE')] / obtained_ie_sum

        Tch = {'N':prob_TN,'EI':prob_TEI,'IE':prob_TIE}
        TChoice = max(Tch, key = Tch.get)


        chiS = (math.pow(diff_n, 2)/expected_n_sum) + (math.pow(diff_ei, 2)/expected_ei_sum) + (math.pow(diff_ie, 2)/expected_ie_sum)

        return (chiS,{'A':AChoice,'C':CChoice,'G':GChoice,'T':TChoice})


"""This function prepares the dataset for our gain, entropy calculation, the return vale of the function
is used to build tree"""
def preprocess_data(k,y,ra,seq_n):
    d_k = []
    for i in range(len(k)):
        d_k.append(list(k[i]))
    
    dna_seq = []
    for j in range(60):
        d_k1 = [i[j] for i in d_k]
        dna_seq.append(d_k1)
        
    #descendant attribute calculation
    if ra > 0:
        d_x = [i for i in dna_seq[ra] if i == seq_n]
        dna_nn =  tuple(zip(d_x,y))
        s = collections.Counter(dna_nn) #calculates IE,EI,N for the sequences
        try:
            s['N'] = s.pop((seq_n,'N'))
            s['EI'] = s.pop((seq_n,'EI'))
            s['IE'] = s.pop((seq_n,'IE'))
        except KeyError:
            s['EI'] = 0
    
        dna_x =[]
        for m in range(60):
            n = zip(dna_seq[ra],dna_seq[m])
            d_y = [j for i,j in n if i == seq_n]
            dna_n = tuple(zip(d_y,y))
            dna_x.append(dna_n)
        return(dna_x,s) #returns the map of dna sequence columnwise with the result
    
    #initial root attribute calculation
    if ra == 0 and seq_n ==0:
        s = collections.Counter(y)
        dna_map = []
        for n in range(60):
            dna_m =  tuple(zip(dna_seq[n],y))
            dna_map.append(dna_m)
        return(dna_map,s)
            
    
"""This function takes root attribute as input, calculates the gain using entropy_IH.py
returns the attribute with higher gain. The return value from the class is a dictionary"""
ra_track = [] #keep track of the root attribute to exclude in calculation of descandant attribute
def root_attr(dna_map,s,ra,u):
    ra_track.append(ra)
    #entropy_calculation
    d = {}
    if u ==1:
        entropy_calc = entropy_IF()
        d,klist = entropy_calc.information_gain(dna_map,s,ra)
    #gini_calculation
    if u == 2:
        gini_calc = gini()
        d,klist = gini_calc.acgt_index(dna_map,s,ra)

    for k in ra_track:
        if k in d:
            del d[k]
    
    if d:
        return (max(d, key = d.get),klist) # returns attribute with maximum information gain
    else:
        return d

"""This funtion recursively called to build the subtrees A,C,G and T. The return
value of the function is the list of attributes of A,C,G and T"""
def build_tree(k,y,ra):
    #create sub_trees of the protein sequence
    p_seq = ['A','C','G','T']
    edge_attr = []
    chi_square = []
    posnodes_list = []
    for i in range(len(p_seq)):
        dna_x,s = preprocess_data(k,y,ra,p_seq[i])
        attribute_list = list(range(60))
        k_e = dict(zip(attribute_list,dna_x))
        ra2,klist = root_attr(k_e,s,ra,u)
        if ra2:
            chi_sq,posnodes = chi(klist[ra2])
            chi_square.append(chi_sq)
            edge_attr.append(ra2)
            posnodes_list.append(posnodes)
    probabli_chi = dict(zip(edge_attr,posnodes_list))
    edge_chi = dict(zip(edge_attr,chi_square))
    return ([edge_chi,probabli_chi,u])

"""recursive function"""
def ret_root_attr(k,y,ra,chi_squ29,StubSed):
    print ("The root attribute is %s" % ra)
    prob = []
    for ke, vl in chi_squ29.items():
        prob.append(vl)
    edge_attr = build_tree(k,y,ra)
    edge_n=[]
    for keys,val in edge_attr[0].items():
        edge_n.append(keys)
        plot_edges.append((ra,keys))
    
    map_e = dict(zip(edge_n,prob))
    p_seq = ['A','C','G','T']
    ec=-1
    for next_root, v in edge_attr[0].items(): 
        ec +=1
        if (u == 1 and v < h) or (u == 2 and v > o):
            StubSed[ra] = p_seq[ec]
            G.add_node(next_root, color='goldenrod2', style='filled')
            G.add_edge(ra,next_root,label=p_seq[ec])
            prob = edge_attr[1][next_root]
            root = ret_root_attr(k,y,next_root,prob,list(StubSed))        
        else: 
            StubSed[ra] = p_seq[ec]
            #regular expression matching command for testing
            SedCmdFile.write("s/^\\([0-9]*,\\)%s/\\1%s/\n" % ("".join(StubSed),map_e[next_root]))
            G.add_edge(ra,map_e[next_root],label=p_seq[ec])
        
        
def main():
    g  = pd.read_csv("training.csv",delimiter=",",header=None)
    g.columns = list(range(0,3))
    # K the 2nd column - the protein sequence 
    k = list(g[1])
    # Total number of N,IE,EI
    y = list(g[2])
    global SedCmdFile
    #sed command file for running against testing.csv
    SedCmdFile = open("SedCMD.sed","w")
    global SedString
    SedString = list(k[0])
    for i in range(len(SedString)):
        SedString[i] = '.'
    print("%s" % "".join(SedString))
    global TotalSum
    TotalSum = collections.Counter(g[2])
    # finding 1st root attribute 29
    ra = 0
    seq_n = 0
    #dna_map - the tuple of a character in a sequence and the ouput EI,IE,N
    dna_map,s = preprocess_data(k,y,ra,seq_n)
    attribute_list = list(range(60))
    #k_e - dictionary of  attribute and 2000 column character sequence
    k_e = dict(zip(attribute_list,dna_map)) 
    #root_attribute - returns the attribute with maximum gain
    global u
    u= int(input("Press 1 for entropy_calculation and 2 for gini calculation: "))
    root_attribute,klist = root_attr(k_e,s,ra,u)
    G.add_node(root_attribute, color='goldenrod2', style='filled',weight=1)
    print ("The 1 root attribute: %s" % root_attribute)
    chi_sq = chi(klist[ra])
    StubSed = list(SedString)
    leaf_node = ret_root_attr(k,y,root_attribute,chi_sq[1],StubSed)
    G.draw("DT.png",prog = "dot") 
    pylab.show()

    SedCmdFile.write("s/^\\([0-9]*,\\)%s/\\1%s/" % ("".join(SedString),"N"))
    SedCmdFile.close()

    print("%s" % "".join(SedString))

        
if __name__ == '__main__':
    main()
