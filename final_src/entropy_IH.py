# -*- coding: utf-8 -*-
"""
Created on Tue Sep 19 03:08:54 2017

@author: Raj, Damion and Dominic
"""
import math
from collections import defaultdict
from collections import Counter
ra_track = []
class entropy_IF:
    # this function log 0 -> infinity problem
    def log_zero(self,x,y,z):
        if x == 0.0:
            x = 1.0
        if y == 0.0: 
            y =1.0
        if z == 0.0: 
            z =1.0
        return x,y,z
    
    def information_gain(self,g,s,ra):
        max_gain = []
        klist = []
        for i in range(len(g)):
            if i == ra :
                k = Counter(g[i])
                klist.append(k)
                continue
            try:
                k = Counter(g[i])
            except KeyError:
                pass
            s = Counter(s)
            
            tot_sum = s['N']+s['EI']+s['IE']
            if tot_sum == 0.0:
                tot_sum =1
            prob_n = s['N']/tot_sum
            prob_ei = s['EI']/tot_sum
            prob_ie = s['IE']/tot_sum
            prob_n,prob_ie,prob_ei = self.log_zero(prob_n, prob_ie, prob_ei)
            entrop_s =  -(prob_n)*math.log2(prob_n)-(prob_ei)*math.log2(prob_ei)-(prob_ie)*math.log2(prob_ie)
        
            totA_sum = k[('A','N')] + k[('A','IE')] + k[('A','EI')]
            if totA_sum ==0.0:
                totA_sum =1
            probA_n = k[('A','N')]/totA_sum
            probA_ie = k[('A','IE')]/totA_sum
            probA_ei = k[('A','EI')]/totA_sum
            probA_n,probA_ie,probA_ei = self.log_zero(probA_n, probA_ie, probA_ei)
            entrop_A = -(probA_n)*math.log2(probA_n)-(probA_ei)*math.log2(probA_ei)-(probA_ie)*math.log2(probA_ie)
            
            totC_sum = k[('C','N')] + k[('C','IE')] + k[('C','EI')]
            if totC_sum ==0.0:
                totC_sum =1
            probC_n = k[('C','N')]/totC_sum
            probC_ie = k[('C','IE')]/totC_sum
            probC_ei = k[('C','EI')]/totC_sum
            probC_n,probC_ie,probC_ei= self.log_zero(probC_n, probC_ie, probC_ei)
            entrop_C = -(probC_n)*math.log2(probC_n)-(probC_ei)*math.log2(probC_ei)-(probC_ie)*math.log2(probC_ie)
             
            totG_sum = k[('G','N')] + k[('G','IE')] + k[('G','EI')]
            if totG_sum ==0.0:
                totG_sum =1
            probG_n = k[('G','N')]/totG_sum
            probG_ie = k[('G','IE')]/totG_sum
            probG_ei = k[('G','EI')]/totG_sum
            probG_n,probG_ie,probG_ei= self.log_zero(probG_n, probG_ie, probG_ei)
            entrop_G = -(probG_n)*math.log2(probG_n)-(probG_ei)*math.log2(probG_ei)-(probG_ie)*math.log2(probG_ie)
                
            totT_sum = k[('T','N')] + k[('T','IE')] + k[('T','EI')]
            if totT_sum ==0.0:
                totT_sum =1
            probT_n = k[('T','N')]/totT_sum
            probT_ie = k[('T','IE')]/totT_sum
            probT_ei = k[('T','EI')]/totT_sum
            probT_n,probT_ie,probT_ei= self.log_zero(probT_n, probT_ie, probT_ei)
            entrop_T = -(probT_n)*math.log2(probT_n)-(probT_ei)*math.log2(probT_ei)-(probT_ie)*math.log2(probT_ie)

            info_gain = entrop_s - (totA_sum/tot_sum)*entrop_A - (totC_sum/tot_sum)*entrop_C - (totG_sum/tot_sum)*entrop_G - (totT_sum/tot_sum)*entrop_T 
            z = (i,info_gain)
            max_gain.append(list(z))
            klist.append(k)
        
        d = defaultdict(list)
        for i in max_gain:
            d[i[0]].append(i[1:])
        return (d,klist) #return dictionary of attribute along with its information gain

    
